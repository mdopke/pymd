#!/usr/bin/env python

import matplotlib
matplotlib.use('Qt5Agg')

import matplotlib.pyplot as plt


def init_plotting():
    '''
    Function used to adjust plot settings.
    Recommended to always call when plotting is involved

    :return:
    '''
    print('Adjusting style!')

    # plt.minorticks_on()
    plt.rcParamsDefault

    plt.style.use('classic')
    plt.rcParams['image.cmap'] = 'jet'  # 'grey'
    # plt.rcParams['axes.facecolor'] = None


    # plt.rcParams['xtick.color'] = 'grey'
    # plt.rcParams['ytick.color'] = 'grey'
    # plt.rcParams['grid.color'] = 'grey'
    # plt.rcParams['grid.linestyle'] = '--'
    # plt.rcParams['grid.linewidth'] = 1
    # plt.rcParams['grid.alpha'] = 1
    # plt.rcParams['axes.grid'] = True
    # plt.rcParams['axes.grid.axis'] = 'both'

    # plt.rcParams['axes.edgecolor'] = 'k'
    # plt.rcParams['axes.linewidth'] = 2

    # plt.rcParams['font.family'] = 'serif'
    # plt.rcParams['font.serif'] = 'Ubuntu'
    # plt.rcParams['font.monospace'] = 'Ubuntu Mono'
    plt.rcParams['font.size'] = 18
    plt.rcParams['legend.fontsize'] = 18
    plt.rcParams['axes.labelsize'] = 20
    # plt.rcParams['axes.labelpad'] = 80

    plt.rcParams['xtick.labelsize'] = 20
    plt.rcParams['ytick.labelsize'] = 20
    plt.rcParams['xtick.direction'] = 'in'
    plt.rcParams['ytick.direction'] = 'in'

    # plt.rcParams['xtick.bottom'] = True
    # plt.rcParams['xtick.top'] = True
    # plt.rcParams['xtick.minor.visible'] = True
    # plt.rcParams['xtick.minor.size'] = 3
    # plt.rcParams['xtick.major.size'] = 5
    # plt.rcParams['xtick.minor.bottom'] = True
    # plt.rcParams['xtick.minor.top'] = True

    # plt.rcParams['ytick.left'] = True
    # plt.rcParams['ytick.right'] = True
    # plt.rcParams['ytick.minor.visible'] = True
    # plt.rcParams['ytick.minor.size'] = 3
    # plt.rcParams['ytick.major.size'] = 5
    # plt.rcParams['ytick.minor.left'] = True
    # plt.rcParams['ytick.minor.right'] = True
    # plt.rcParams['hatch.linewidth'] = 4.0

    # # plt.rcParams['xtick.major.size'] = 10
    # # plt.rcParams['xtick.minor.size'] = 5
    # # plt.rcParams['ytick.major.size'] = 10
    # # plt.rcParams['ytick.minor.size'] = 5

    plt.rcParams['lines.markeredgewidth'] = 1
    plt.rcParams['lines.markersize'] = 6
    plt.rcParams['lines.linewidth'] = 1

    plt.rcParams['patch.linewidth'] = 1
    # plt.rcParams['markers.fillstyle'] = 'none'

    plt.rcParams['figure.autolayout']=False
    plt.rcParams['figure.figsize'] = 9, 6#6,4
    plt.rcParams['figure.subplot.bottom'] = 0.18
    plt.rcParams['figure.subplot.hspace'] = 0.2
    plt.rcParams['figure.subplot.left'] = 0.18
    plt.rcParams['figure.subplot.right'] = 0.95
    plt.rcParams['figure.subplot.top'] = 0.95
    plt.rcParams['figure.subplot.wspace'] = 0.2
    # plt.rcParams['image.aspect'] = 'equal'

    plt.rcParams['legend.frameon'] = False
    plt.rcParams['legend.numpoints'] = 1
    plt.rcParams['legend.markerscale'] = 0.8

    plt.rcParams['legend.framealpha'] = 0.0
    plt.rcParams['legend.handlelength'] = 1
    plt.rcParams['legend.labelspacing'] = 0.2
    plt.rcParams['legend.handletextpad'] = 0.3
    plt.rcParams['legend.borderaxespad'] = 0.3
    plt.rcParams['legend.columnspacing'] = 0.8
    # plt.rcParams['grid.alpha'] = 0.5

