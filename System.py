#!/usr/bin/env

import numpy
import math
import copy
from MDAnalysis import Universe


#TODO important, changed empty to zeros

class Atom():

    def __init__(self, symb=None, molid=int(), index=int(), type=int(), mass=float(), charge=float(),
                 pos=numpy.array([], dtype=float), vel=numpy.array([], dtype=float), force=numpy.array([], dtype=float),
                 comment=None):

        self.index = index
        self.molid = molid
        self.symb = symb
        self.type = type
        self.mass = mass
        self.charge = charge
        self.pos = pos
        self.vel = vel
        self.force = force

        if comment:
            self.comment = comment
        else:
            self.comment = symb

    def __repr__(self):
        return '%s' % self.symb


class Box():

    def __init__(self):

        self.mins = numpy.zeros(shape=3, dtype='float')
        self.maxs = numpy.zeros(shape=3, dtype='float')
        self.lengths = numpy.zeros(shape=3, dtype='float')
        self.center = numpy.zeros(shape=3, dtype='float')


    def __repr__(self):
        rep = '%.4f %.4f xlo xhi\n%.4f %.4f ylo yhi\n%.4f %.4f zlo zhi\n' % (
            self.mins[0], self.maxs[0], self.mins[1], self.maxs[1],
            self.mins[2], self.maxs[2])
        rep = ''.join([rep, 'Lx = %.4f, Ly = %.4f, Lz = %.4f' % (
            self.lengths[0], self.lengths[1], self.lengths[2])])
        return rep

    def volume(self):
        return numpy.prod(self.lengths)


class System():

    def __init__(self, name=None, n_atoms=0, n_bonds=0, n_angles=0, n_dihedrals=0, n_impropers=0):

        # simulation box
        self.box            = Box()

        # name
        self.name = name

        # mass
        self.mass_dict      = dict()
        self.type_dict      = dict()

        # per atom
        self.indices        = numpy.zeros(shape=n_atoms, dtype='int')
        self.molids         = numpy.zeros(shape=n_atoms, dtype='int')
        self.types          = numpy.zeros(shape=n_atoms, dtype='int')

        self.charges        = numpy.zeros(shape=n_atoms, dtype='float')
        self.pos            = numpy.zeros(shape=(n_atoms, 3), dtype='float')
        self.vels           = numpy.zeros(shape=(n_atoms, 3), dtype='float')
        self.forces           = numpy.zeros(shape=(n_atoms, 3), dtype='float')

        self.masses         = numpy.zeros(shape=n_atoms, dtype='float')

        # connectivity
        self.bonds          = numpy.zeros(shape=(n_bonds, 3), dtype='int')
        self.angles         = numpy.zeros(shape=(n_angles, 4), dtype='int')
        self.dihedrals      = numpy.zeros(shape=(n_dihedrals, 5), dtype='int')
        self.impropers      = numpy.zeros(shape=(n_impropers, 5), dtype='int')

        # comments
        self.atoms_comments = numpy.zeros(shape=n_atoms, dtype='object')
        self.molids_comments = numpy.zeros(shape=n_atoms, dtype='object')
        self.bonds_comments = numpy.zeros(shape=n_bonds, dtype='object')
        self.angles_comments = numpy.zeros(shape=n_angles, dtype='object')
        self.dihedrals_comments = numpy.zeros(shape=n_dihedrals, dtype='object')
        self.impropers_comments = numpy.zeros(shape=n_impropers, dtype='object')

        # trajectory
        self.universe = Universe()


    def __repr__(self):

        rep = ''
        if self.name:
            rep += '%s' % self.name
        elif self.type_dict:
            rep += '%s' % self.type_dict
        elif self.mass_dict:
            rep += '%s' % self.mass_dict
        else:
            rep += 'No information available'

        return rep

    def update(self, n_atoms=0, n_bonds=0, n_angles=0, n_dihedrals=0, n_impropers=0):

        if n_atoms != 0:
            # per atom
            self.indices = numpy.zeros(shape=n_atoms, dtype='int')
            self.molids = numpy.zeros(shape=n_atoms, dtype='int')
            self.types = numpy.zeros(shape=n_atoms, dtype='int')

            self.charges = numpy.zeros(shape=n_atoms, dtype='float')
            self.pos = numpy.zeros(shape=(n_atoms, 3), dtype='float')
            self.vels = numpy.zeros(shape=(n_atoms, 3), dtype='float')
            self.forces = numpy.zeros(shape=(n_atoms, 3), dtype='float')

            self.masses = numpy.zeros(shape=n_atoms, dtype='float')

            self.atoms_comments = numpy.zeros(shape=n_atoms, dtype='object')

        if n_bonds != 0:
            self.bonds = numpy.zeros(shape=(n_bonds, 3), dtype='int')
            self.bonds_comments = numpy.zeros(shape=n_bonds, dtype='object')

        if n_angles != 0:
            self.angles = numpy.zeros(shape=(n_angles, 4), dtype='int')
            self.angles_comments = numpy.zeros(shape=n_angles, dtype='object')

        if n_dihedrals != 0:
            self.dihedrals = numpy.zeros(shape=(n_dihedrals, 5), dtype='int')
            self.dihedrals_comments = numpy.zeros(shape=n_dihedrals, dtype='object')

        if n_impropers != 0:
            self.impropers = numpy.zeros(shape=(n_impropers, 5), dtype='int')
            self.impropers_comments = numpy.zeros(shape=n_impropers, dtype='object')


    def n_atoms(self):
        return len(self.pos)

    def n_mols(self):
        if len(self.molids) == 0:
            return 0
        else:
            return len(numpy.unique(self.molids))

    def n_bonds(self):
        return len(self.bonds)

    def n_angles(self):
        return len(self.angles)

    def n_dihedrals(self):
        return len(self.dihedrals)

    def n_impropers(self):
        return len(self.impropers)

    def n_molTypes(self):
        if len(self.molids) == 0:
            return 0
        else:
            return int(max(numpy.unique(self.molids)))

    def n_atomTypes(self):
        if len(self.types) == 0:
            return 0
        else:
            return int(max(numpy.unique(self.types)))

    def n_bondTypes(self):
        if len(self.bonds) == 0:
            return 0
        else:
            return int(max(numpy.unique(self.bonds[:, 0])))

    def n_angleTypes(self):
        if len(self.angles) == 0:
            return 0
        else:
            return int(max(numpy.unique(self.angles[:, 0])))

    def n_dihedralTypes(self):
        if len(self.dihedrals) == 0:
            return 0
        else:
            return int(max(numpy.unique(self.dihedrals[:, 0])))

    def n_improperTypes(self):
        if len(self.impropers) == 0:
            return 0
        else:
            return int(max(numpy.unique(self.impropers[:, 0])))

    def atomTypes(self):
        if len(self.types) == 0:
            return None
        else:
            return numpy.unique(self.types)

    def bondTypes(self):
        if len(self.bonds) == 0:
            return None
        else:
            return numpy.unique(self.bonds[:, 0])

    def angleTypes(self):
        if len(self.angles) == 0:
            return None
        else:
            return numpy.unique(self.angles[:, 0])

    def dihedralTypes(self):
        if len(self.dihedrals) == 0:
            return None
        else:
            return numpy.unique(self.dihedrals[:, 0])

    def improperTypes(self):
        if len(self.impropers) == 0:
            return None
        else:
            return numpy.unique(self.impropers[:, 0])

    # functions
    def translate(self, coords=numpy.zeros(shape=3, dtype='float')):
        self.pos += coords

    def delete_atom(self, atom_index):

        # delete per atom
        self.indices = numpy.delete(self.indices, atom_index-1)
        self.molids = numpy.delete(self.molids, atom_index-1)
        self.types = numpy.delete(self.types, atom_index-1)
        self.charges = numpy.delete(self.charges, atom_index-1)
        self.pos = numpy.delete(self.pos, atom_index-1, axis=0)
        self.vels = numpy.delete(self.vels, atom_index-1, axis=0)
        self.forces = numpy.delete(self.forces, atom_index-1, axis=0)
        self.masses = numpy.delete(self.masses, atom_index-1)
        self.atoms_comments = numpy.delete(self.atoms_comments, atom_index-1)

        # delete connectivities
        bond_index = []
        for i, bond in enumerate(self.bonds):
            if atom_index in bond[1:]:
                bond_index.append(i)

        self.delete_bond(bond_index)

        angle_index = []
        for i, angle in enumerate(self.angles):
            if atom_index in angle[1:]:
                angle_index.append(i)

        self.delete_angle(angle_index)

        dihedral_index = []
        for i, dihedral in enumerate(self.dihedrals):
            if atom_index in dihedral[1:]:
                dihedral_index.append(i)

        self.delete_dihedral(dihedral_index)

        improper_index = []
        for i, improper in enumerate(self.impropers):
            if atom_index in improper[1:]:
                improper_index.append(i)

        self.delete_improper(dihedral_index)

        # update indices
        for i, index in enumerate(self.indices):
            if index > atom_index:
                self.indices[i] -= 1

        for i, bond in enumerate(self.bonds):
            for j in range(2):
                if bond[j + 1] > atom_index:
                    self.bonds[i, j + 1] -= 1

        for i, angle in enumerate(self.angles):
            for j in range(3):
                if angle[j + 1] > atom_index:
                    self.angles[i, j + 1] -= 1

        for i, dihedral in enumerate(self.dihedrals):
            for j in range(4):
                if dihedral[j + 1] > atom_index:
                    self.dihedrals[i, j + 1] -= 1

        for i, improper in enumerate(self.impropers):
            for j in range(5):
                if improper[j + 1] > atom_index:
                    self.impropers[i, j + 1] -= 1

        # check dictionaries
        print('If all of one type are removed, the type will remain in the system '
              'The type needs to be removed and shifted manually.\r', end='')


    def delete_bond(self, index):
        self.bonds = numpy.delete(self.bonds, index, axis=0)
        self.bonds_comments = numpy.delete(self.bonds_comments, index)


    def delete_angle(self, index):
        self.angles = numpy.delete(self.angles, index, axis=0)
        self.angles_comments = numpy.delete(self.angles_comments, index)


    def delete_dihedral(self, index):
        self.dihedrals = numpy.delete(self.dihedrals, index, axis=0)
        self.dihedrals_comments = numpy.delete(self.dihedrals_comments, index)


    def delete_improper(self, index):
        self.impropers = numpy.delete(self.impropers, index, axis=0)
        self.impropers_comments = numpy.delete(self.impropers_comments, index)


    def new_type(self, type=0, symb='X', mass=0):

        if type in self.mass_dict.keys():
            print('Type already exists, not doing anything\r', end='')
        else:
            self.mass_dict[type] = mass
            self.type_dict[type] = symb


    def insert_atom(self, atom=Atom(), symb=None, molid=int(), index=int(), type=int(), mass=float(), charge=float(),
                 pos=numpy.array([], dtype=float), vel=numpy.array([], dtype=float), force=numpy.array([], dtype=float),
                    comment=None):

        if type:
            type_ = type
        elif atom.type:
            type_ = atom.type
        else:
            type_ = self.n_atomTypes() + 1

        if symb:
            symb_ = symb
        elif atom.symb:
            symb_ = atom.symb
        else:
            symb_ = 'X'

        if mass:
            mass_ = mass
        elif atom.mass:
            mass_ = atom.mass
        else:
            mass_ = 0

        self.new_type(type_, symb_, mass_)

        if molid:
            self.molids = numpy.hstack((self.molids, molid))
        elif atom.molid:
            self.molids = numpy.hstack((self.molids, atom.molid))
        else:
            self.molids = numpy.hstack((self.molids, self.n_molTypes() + 1))

        if index:
            self.indices = numpy.hstack((self.indices, index))
        elif atom.index:
            self.indices = numpy.hstack((self.indices, atom.index))
        else:
            self.indices = numpy.hstack((self.indices, self.n_atoms() + 1))

        if type:
            self.types = numpy.hstack((self.types, type))
        elif atom.type:
            self.types = numpy.hstack((self.types, atom.type))
        else:
            self.types = numpy.hstack((self.types, self.n_atomTypes() + 1))

        if mass:
            self.masses = numpy.hstack((self.masses, mass))
        elif atom.mass:
            self.masses = numpy.hstack((self.masses, atom.mass))
        else:
            self.masses = numpy.hstack((self.masses, 0))

        if charge:
            self.charges = numpy.hstack((self.charges, charge))
        elif atom.charge:
            self.charges = numpy.hstack((self.charges, atom.charge))
        else:
            self.charges = numpy.hstack((self.charges, 0))

        if pos.size > 0:
            self.pos = numpy.vstack((self.pos, pos))
        elif atom.pos.size > 0:
            self.pos = numpy.vstack((self.pos, atom.pos))
        else:
            self.pos = numpy.vstack((self.pos, numpy.zeros(shape=(1, 3), dtype=float)))

        if vel.size > 0:
            self.vels = numpy.vstack((self.vels, vel))
        elif atom.vel.size > 0:
            self.vels = numpy.vstack((self.vels, atom.vel))
        else:
            self.vels = numpy.vstack((self.vels, numpy.zeros(shape=(1, 3), dtype=float)))

        if force.size > 0:
            self.forces = numpy.vstack((self.forces, force))
        elif atom.force.size > 0:
            self.forces = numpy.vstack((self.forces, atom.force))
        else:
            self.forces = numpy.vstack((self.forces, numpy.zeros(shape=(1, 3), dtype=float)))

        if comment:
            self.atoms_comments = numpy.hstack((self.atoms_comments, comment))
        elif atom.comment:
            self.atoms_comments = numpy.hstack((self.atoms_comments, atom.comment))
        elif symb:
            self.atoms_comments = numpy.hstack((self.atoms_comments, symb))
        elif atom.symb:
            self.atoms_comments = numpy.hstack((self.atoms_comments, atom.symb))
        else:
            self.atoms_comments = numpy.hstack((self.atoms_comments, None))



    # def insert_atom(self, symb=None, molid=0, type=0, mass=0, charge=0, pos=numpy.array([0, 0, 0]), vel=numpy.array([0, 0, 0]), comment='None'):
    #
    #
    #     self.new_type(type, symb, mass)
    #     if molid == 0:
    #         molid = numpy.max(self.molids)+1
    #     if type == 0:
    #         type = numpy.max(self.types)+1
    #
    #     self.indices = numpy.hstack((self.indices, self.n_atoms()+1))
    #     self.molids = numpy.hstack((self.molids, molid))
    #     self.types = numpy.hstack((self.types, type))
    #     self.charges = numpy.hstack((self.charges, charge))
    #     self.pos = numpy.vstack((self.pos, pos.reshape(1, 3)))
    #     self.vels = numpy.vstack((self.vels, vels.reshape(1, 3)))
    #     self.masses = numpy.hstack((self.masses, mass))
    #     self.atoms_comments = numpy.hstack((self.atoms_comments, comment))


    def insert_bond(self, type, index1, index2):
        self.bonds = numpy.vstack((self.bonds, numpy.array([type, index1, index2])))

    def insert_angle(self, type, index1, index2, index3):
        self.angles = numpy.vstack((self.angles, numpy.array([type, index1, index2, index3])))

    def change_atomType(self, oldtype, newtype):

        self.types = numpy.where(self.types == oldtype, newtype, self.types)
        if oldtype in self.mass_dict:
            self.mass_dict[newtype] = self.mass_dict[oldtype]
            del self.mass_dict[oldtype]
        if oldtype in self.type_dict:
            self.type_dict[newtype] = self.type_dict[oldtype]
            del self.type_dict[oldtype]

    def change_bondType(self, oldtype, newtype):

        self.bonds[:, 0] = numpy.where(self.bonds[:, 0] == oldtype, newtype, self.bonds[:, 0])

    def change_angleType(self, oldtype, newtype):

        self.angles[:, 0] = numpy.where(self.angles[:, 0] == oldtype, newtype, self.angles[:, 0])

    def change_dihedralType(self, oldtype, newtype):

        self.dihedrals[:, 0] = numpy.where(self.dihedrals[:, 0] == oldtype, newtype, self.dihedrals[:, 0])

    def change_improperType(self, oldtype, newtype):

        self.impropers[:, 0] = numpy.where(self.impropers[:, 0] == oldtype, newtype, self.impropers[:, 0])


    def wrap(self):
        """Wrap coordinates into box"""
        # for i, coords in enumerate(self.pos):

        n = numpy.floor((self.box.mins - self.pos)/self.box.lengths) + 1
        self.pos += self.box.lengths * n

        # for i, coords in enumerate(self.pos):
        #     for k, c in enumerate(coords):
        #         if c < self.box.mins[k]:
        #             n = math.floor((self.box.mins[k] - c)/self.box.lengths[k]) + 1
        #             self.pos[i, k] += self.box.lengths[k] * n
        #         elif c > self.box.maxs[k]:
        #             n = math.floor((c - self.box.maxs[k])/self.box.lengths[k]) + 1
        #             self.pos[i, k] -= self.box.lengths[k] * n

    def calc_center(self):
        return numpy.sum(self.pos, axis=0)/self.n_atoms()

    def combine_systems(self, molecule, box=None, center=None):

        if self.n_atoms() == 0:

            shift = center - molecule.calc_center()
            pos = molecule.pos + shift

            # simulation box
            self.box = molecule.box

            # mass
            self.mass_dict = molecule.mass_dict
            self.type_dict = molecule.type_dict

            # per atom
            self.indices = molecule.indices
            self.molids = molecule.molids
            self.types = molecule.types

            self.charges = molecule.charges
            self.pos = pos
            self.vels = molecule.vels
            self.forces = molecule.forces

            self.masses = molecule.masses

            # connectivity
            self.bonds = molecule.bonds
            self.angles = molecule.angles
            self.dihedrals = molecule.dihedrals
            self.impropers = molecule.impropers

            # comments
            self.atoms_comments = molecule.atoms_comments
            self.bonds_comments = molecule.bonds_comments
            self.angles_comments = molecule.angles_comments
            self.dihedrals_comments = molecule.dihedrals_comments
            self.impropers_comments = molecule.impropers_comments

        else:

            if box is not None:
                self.box = box

            for type in molecule.types:
                self.new_type(type)#, molecule.type_dict[type], molecule.mass_dict[type])

            if center is not None:
                shift = center - molecule.calc_center()
                pos = molecule.pos + shift
            else:
                pos = molecule.pos


            # connectivity
            bonds = copy.deepcopy(molecule.bonds)
            bonds[:, 1:] += self.n_atoms()
            angles = copy.deepcopy(molecule.angles)
            angles[:, 1:] += self.n_atoms()
            dihedrals = copy.deepcopy(molecule.dihedrals)
            dihedrals[:, 1:] += self.n_atoms()
            impropers = copy.deepcopy(molecule.impropers)
            impropers[:, 1:] += self.n_atoms()

            self.bonds = numpy.vstack((self.bonds, bonds))
            self.angles = numpy.vstack((self.angles, angles))
            self.dihedrals = numpy.vstack((self.dihedrals, dihedrals))
            self.impropers = numpy.vstack((self.impropers, impropers))

            # per atom
            self.indices = numpy.hstack((self.indices, molecule.indices+self.n_atoms()))
            self.molids = numpy.hstack((self.molids, molecule.molids+self.n_molTypes()))
            self.types = numpy.hstack((self.types, molecule.types))
            self.charges = numpy.hstack((self.charges, molecule.charges))
            self.pos = numpy.vstack((self.pos, pos))
            self.vels = numpy.vstack((self.vels, molecule.vels))
            self.forces = numpy.vstack((self.forces, molecule.forces))
            self.masses = numpy.hstack((self.masses, molecule.masses))
            self.atoms_comments = numpy.hstack((self.atoms_comments, molecule.atoms_comments))


            # comments
            self.bonds_comments = numpy.hstack((self.bonds_comments, molecule.bonds_comments))
            self.angles_comments = numpy.hstack((self.angles_comments, molecule.angles_comments))
            self.dihedrals_comments = numpy.hstack((self.dihedrals_comments, molecule.dihedrals_comments))
            self.impropers_comments = numpy.hstack((self.impropers_comments, molecule.impropers_comments))

    def rescale(self, boxNew):

        fac = boxNew.lengths/self.box.lengths

        self.pos *= fac
        self.box = boxNew

    def repair_molecules(self, resids):

        # find all atoms with given resids
        for resid in resids:
            where_resid = numpy.where(self.molids == resid)[0]

            # check if molecule length is < 2:
            if len(where_resid) >= 2:

                # grab first instance of resid as reference position and calculate distances
                d = self.pos[where_resid[0], :] - self.pos[where_resid[1:], :]

                if (numpy.abs(d) > self.box.lengths/2).any():

                    indices = numpy.where(d >  self.box.lengths/2)
                    self.pos[where_resid[1:][indices[0]], indices[1]] += self.box.lengths[indices[1]]

                    indices = numpy.where(d < -self.box.lengths/2)
                    self.pos[where_resid[1:][indices[0]], indices[1]] -= self.box.lengths[indices[1]]


    def bounding_box(self, selection):

        bbox = Box()

        bbox.maxs[0] = numpy.max(self.pos[selection, 0])
        bbox.mins[0] = numpy.min(self.pos[selection, 0])

        bbox.maxs[1] = numpy.max(self.pos[selection, 1])
        bbox.mins[1] = numpy.min(self.pos[selection, 1])

        bbox.maxs[2] = numpy.max(self.pos[selection, 2])
        bbox.mins[2] = numpy.min(self.pos[selection, 2])

        bbox.lengths = bbox.maxs - bbox.mins

        return bbox

