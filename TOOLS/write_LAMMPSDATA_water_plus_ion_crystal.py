#!/usr/bin/env

import numpy
from PyMD.IO import write_lammpsdata, read_lammpsdata
from PyMD.functions import add_water, minimize
from PyMD.System import System, Box, Atom
import random
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--fout', type=str)
    parser.add_argument('--style', type=str)
    parser.add_argument('--m', type=float)
    parser.add_argument('--dens', type=float)
    parser.add_argument('--LC', type=float)
    parser.add_argument('--LN', type=int)
    parser.add_argument('--Lbox', metavar='N', type=float,
                        help='Box length')

    args = parser.parse_args()

    if args.style == None:
        args.style = 'default'


    system = System()

    # make crystal
    Na = Atom(symb='Na', type=3, mass=22.990, charge=+1)
    Cl = Atom(symb='Cl', type=4, mass=35.45, charge=+1)

    Lbox = float(args.Lbox)/2
    m = float(args.m)
    LC = float(args.LC)
    num = int(args.LN) # has to be even
    dens = float(args.dens)

    print('Size of crystal is %.2f ** 3' % ((num-1) * LC))
    if Lbox <= (num-1) * LC:
        print('Lbox too small')
        print('Minimum of %f is required' % (2*(num-1) * LC))
        exit()


    NA = 6.0221415e23  # 1/mol
    xyz = numpy.zeros((int(num**3), 3))
    c = 0
    for i in range(num):
        for j in range(num):
            for k in range(num):
                xyz = numpy.array([0, 0, 0]) + numpy.array([i, j, k]) * LC - 0.5 * (num-1) * LC
                if c % 2 == 0:
                    system.insert_atom(atom=Na, pos=xyz)
                else:
                    system.insert_atom(atom=Cl, pos=xyz)
                c += 1
                if k == num - 1 and not j == num - 1:
                    c -= 1

    # ------------------------------- #
    #                 |
    #                 |
    #                 |
    #                 |
    #             ----|----
    #            |    |    |
    #            |    |    |
    # ================================ #
    #            |    |    |
    #            |    |    |
    #             ----|----
    #                 |
    #                 |
    # --------------------------------- #

    ion_counter = 0

    # region 1 all left
    region = Box()
    region.mins = numpy.array([-Lbox, -Lbox, -Lbox])
    region.maxs = numpy.array([(- 0.5 * (num-1) * LC), Lbox, Lbox])
    region.lengths = region.maxs - region.mins
    system = add_water(dens, region, system=system)

    # add ions
    Ni = int(numpy.ceil(m * region.volume() * 1e-27 * NA))
    ion_counter += Ni
    for i in range(Ni):
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Na, pos=xyz)
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Cl, pos=xyz)


    # region 2 all right
    region = Box()
    region.mins = numpy.array([(0.5 * (num-1) * LC), -Lbox, -Lbox])
    region.maxs = numpy.array([Lbox, Lbox, Lbox])
    region.lengths = region.maxs - region.mins
    system = add_water(dens, region, system=system)

    # add ions
    Ni = int(numpy.ceil(m * region.volume() * 1e-27 * NA))
    ion_counter += Ni
    for i in range(Ni):
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Na, pos=xyz)
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Cl, pos=xyz)

    # region 3 cube top
    region = Box()
    region.mins = numpy.array([(-0.5 * (num - 1) * LC), (0.5 * (num - 1) * LC), -Lbox])
    region.maxs = numpy.array([(0.5 * (num - 1) * LC), Lbox, Lbox])
    region.lengths = region.maxs - region.mins
    system = add_water(1, region, system=system)

    # add ions
    Ni = int(numpy.ceil(m * region.volume() * 1e-27 * NA))
    ion_counter += Ni
    for i in range(Ni):
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Na, pos=xyz)
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Cl, pos=xyz)


    # region 4 cube bot
    region = Box()
    region.mins = numpy.array([(-0.5 * (num - 1) * LC), -Lbox, -Lbox])
    region.maxs = numpy.array([(0.5 * (num - 1) * LC), (-0.5 * (num - 1) * LC), Lbox])
    region.lengths = region.maxs - region.mins
    system = add_water(1, region, system=system)

    # add ions
    Ni = int(numpy.ceil(m * region.volume() * 1e-27 * NA))
    ion_counter += Ni
    for i in range(Ni):
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Na, pos=xyz)
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Cl, pos=xyz)

    # region 5 cube front
    region = Box()
    region.mins = numpy.array([(-0.5 * (num - 1) * LC), (-0.5 * (num - 1) * LC), (0.5 * (num - 1) * LC)])
    region.maxs = numpy.array([(0.5 * (num - 1) * LC), (0.5 * (num - 1) * LC), Lbox])
    region.lengths = region.maxs - region.mins
    system = add_water(1, region, system=system)

    # add ions
    Ni = int(numpy.ceil(m * region.volume() * 1e-27 * NA))
    ion_counter += Ni
    for i in range(Ni):
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Na, pos=xyz)
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Cl, pos=xyz)

    # region 4 cube bot
    region = Box()
    region.mins = numpy.array([(-0.5 * (num - 1) * LC), (-0.5 * (num - 1) * LC), -Lbox])
    region.maxs = numpy.array([(0.5 * (num - 1) * LC), (0.5 * (num - 1) * LC), (-0.5 * (num - 1) * LC)])
    region.lengths = region.maxs - region.mins
    system = add_water(1, region, system=system)

    # add ions
    Ni = int(numpy.ceil(m * region.volume() * 1e-27 * NA))
    ion_counter += Ni
    for i in range(Ni):
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Na, pos=xyz)
        xyz0 = numpy.array([random.uniform(region.mins[0], region.maxs[0]),
                            random.uniform(region.mins[1], region.maxs[1]),
                            random.uniform(region.mins[2], region.maxs[2])])
        xyz = minimize(system, xyz0, maxIter=200, region='local')
        system.insert_atom(atom=Cl, pos=xyz)

    print('Added %d ion pairs' % ion_counter)

    # final box for file
    region = Box()
    region.mins = numpy.array([-Lbox, -Lbox, -Lbox])
    region.maxs = numpy.array([Lbox, Lbox, Lbox])
    region.lengths = region.maxs - region.mins
    system.box = region

    write_lammpsdata(args.fout, system, style=args.style)