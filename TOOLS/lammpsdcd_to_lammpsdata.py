#!/usr/bin/env

# load system class

import numpy
import math
import copy
import re
import warnings
import os
import psutil
import sys
from MDAnalysis import Universe
import argparse





#TODO important, changed empty to zeros

class Atom():

    def __init__(self, symb=None, molid=int(), index=int(), type=int(), mass=float(), charge=float(),
                 pos=numpy.array([], dtype=float), vel=numpy.array([], dtype=float), force=numpy.array([], dtype=float),
                 comment=None):

        self.index = index
        self.molid = molid
        self.symb = symb
        self.type = type
        self.mass = mass
        self.charge = charge
        self.pos = pos
        self.vel = vel
        self.force = force

        if comment:
            self.comment = comment
        else:
            self.comment = symb

    def __repr__(self):
        return '%s' % self.symb


class Box():

    def __init__(self):

        self.mins = numpy.zeros(shape=3, dtype='float')
        self.maxs = numpy.zeros(shape=3, dtype='float')
        self.lengths = numpy.zeros(shape=3, dtype='float')
        self.center = numpy.zeros(shape=3, dtype='float')


    def __repr__(self):
        rep = '%.4f %.4f xlo xhi\n%.4f %.4f ylo yhi\n%.4f %.4f zlo zhi\n' % (
            self.mins[0], self.maxs[0], self.mins[1], self.maxs[1],
            self.mins[2], self.maxs[2])
        rep = ''.join([rep, 'Lx = %.4f, Ly = %.4f, Lz = %.4f' % (
            self.lengths[0], self.lengths[1], self.lengths[2])])
        return rep


class System():

    def __init__(self, name=None, n_atoms=0, n_bonds=0, n_angles=0, n_dihedrals=0, n_impropers=0):

        # simulation box
        self.box            = Box()

        # name
        self.name = name

        # mass
        self.mass_dict      = dict()
        self.type_dict      = dict()

        # per atom
        self.indices        = numpy.zeros(shape=n_atoms, dtype='int')
        self.molids         = numpy.zeros(shape=n_atoms, dtype='int')
        self.types          = numpy.zeros(shape=n_atoms, dtype='int')

        self.charges        = numpy.zeros(shape=n_atoms, dtype='float')
        self.pos            = numpy.zeros(shape=(n_atoms, 3), dtype='float')
        self.vels           = numpy.zeros(shape=(n_atoms, 3), dtype='float')
        self.forces           = numpy.zeros(shape=(n_atoms, 3), dtype='float')

        self.masses         = numpy.zeros(shape=n_atoms, dtype='float')

        # connectivity
        self.bonds          = numpy.zeros(shape=(n_bonds, 3), dtype='int')
        self.angles         = numpy.zeros(shape=(n_angles, 4), dtype='int')
        self.dihedrals      = numpy.zeros(shape=(n_dihedrals, 5), dtype='int')
        self.impropers      = numpy.zeros(shape=(n_impropers, 5), dtype='int')

        # comments
        self.atoms_comments = numpy.zeros(shape=n_atoms, dtype='object')
        self.bonds_comments = numpy.zeros(shape=n_bonds, dtype='object')
        self.angles_comments = numpy.zeros(shape=n_angles, dtype='object')
        self.dihedrals_comments = numpy.zeros(shape=n_dihedrals, dtype='object')
        self.impropers_comments = numpy.zeros(shape=n_impropers, dtype='object')


    def __repr__(self):

        rep = ''
        if self.name:
            rep += '%s' % self.name
        elif self.type_dict:
            rep += '%s' % self.type_dict
        elif self.mass_dict:
            rep += '%s' % self.mass_dict
        else:
            rep += 'No information available'

        return rep

    def update(self, n_atoms=0, n_bonds=0, n_angles=0, n_dihedrals=0, n_impropers=0):

        if n_atoms != 0:
            # per atom
            self.indices = numpy.zeros(shape=n_atoms, dtype='int')
            self.molids = numpy.zeros(shape=n_atoms, dtype='int')
            self.types = numpy.zeros(shape=n_atoms, dtype='int')

            self.charges = numpy.zeros(shape=n_atoms, dtype='float')
            self.pos = numpy.zeros(shape=(n_atoms, 3), dtype='float')
            self.vels = numpy.zeros(shape=(n_atoms, 3), dtype='float')
            self.forces = numpy.zeros(shape=(n_atoms, 3), dtype='float')

            self.masses = numpy.zeros(shape=n_atoms, dtype='float')

            self.atoms_comments = numpy.zeros(shape=n_atoms, dtype='object')

        if n_bonds != 0:
            self.bonds = numpy.zeros(shape=(n_bonds, 3), dtype='int')
            self.bonds_comments = numpy.zeros(shape=n_bonds, dtype='object')

        if n_angles != 0:
            self.angles = numpy.zeros(shape=(n_angles, 4), dtype='int')
            self.angles_comments = numpy.zeros(shape=n_angles, dtype='object')

        if n_dihedrals != 0:
            self.dihedrals = numpy.zeros(shape=(n_dihedrals, 5), dtype='int')
            self.dihedrals_comments = numpy.zeros(shape=n_dihedrals, dtype='object')

        if n_impropers != 0:
            self.impropers = numpy.zeros(shape=(n_impropers, 5), dtype='int')
            self.impropers_comments = numpy.zeros(shape=n_impropers, dtype='object')


    def n_atoms(self):
        return len(self.pos)

    def n_mols(self):
        return len(numpy.unique(self.molids))

    def n_bonds(self):
        return len(self.bonds)

    def n_angles(self):
        return len(self.angles)

    def n_dihedrals(self):
        return len(self.dihedrals)

    def n_impropers(self):
        return len(self.impropers)

    def n_molTypes(self):
        return int(max(numpy.unique(self.molids)))

    def n_atomTypes(self):
        return int(max(numpy.unique(self.types)))

    def n_bondTypes(self):
        return int(max(numpy.unique(self.bonds[:, 0])))

    def n_angleTypes(self):
        return int(max(numpy.unique(self.angles[:, 0])))

    def n_dihedralTypes(self):
        return int(max(numpy.unique(self.dihedrals[:, 0])))

    def n_improperTypes(self):
        return int(max(numpy.unique(self.impropers[:, 0])))

    def atomTypes(self):
        return numpy.unique(self.types)

    def bondTypes(self):
        return numpy.unique(self.bonds[:, 0])

    def angleTypes(self):
        return numpy.unique(self.angles[:, 0])

    def dihedralTypes(self):
        return numpy.unique(self.dihedrals[:, 0])

    def improperTypes(self):
        return numpy.unique(self.impropers[:, 0])

    # functions
    def translate(self, coords=numpy.zeros(shape=3, dtype='float')):
        self.pos += coords

    def delete_atom(self, atom_index):

        # delete per atom
        self.indices = numpy.delete(self.indices, atom_index-1)
        self.molids = numpy.delete(self.molids, atom_index-1)
        self.types = numpy.delete(self.types, atom_index-1)
        self.charges = numpy.delete(self.charges, atom_index-1)
        self.pos = numpy.delete(self.pos, atom_index-1, axis=0)
        self.vels = numpy.delete(self.vels, atom_index-1, axis=0)
        self.forces = numpy.delete(self.vels, atom_index-1, axis=0)
        self.masses = numpy.delete(self.masses, atom_index-1)
        self.atoms_comments = numpy.delete(self.atoms_comments, atom_index-1)

        # delete connectivities
        bond_index = []
        for i, bond in enumerate(self.bonds):
            if atom_index in bond[1:]:
                bond_index.append(i)

        self.delete_bond(bond_index)

        angle_index = []
        for i, angle in enumerate(self.angles):
            if atom_index in angle[1:]:
                angle_index.append(i)

        self.delete_angle(angle_index)

        dihedral_index = []
        for i, dihedral in enumerate(self.dihedrals):
            if atom_index in dihedral[1:]:
                dihedral_index.append(i)

        self.delete_dihedral(dihedral_index)

        improper_index = []
        for i, improper in enumerate(self.impropers):
            if atom_index in improper[1:]:
                improper_index.append(i)

        self.delete_improper(dihedral_index)

        # update indices
        for i, index in enumerate(self.indices):
            if index > atom_index:
                self.indices[i] -= 1

        for i, bond in enumerate(self.bonds):
            for j in range(2):
                if bond[j + 1] > atom_index:
                    self.bonds[i, j + 1] -= 1

        for i, angle in enumerate(self.angles):
            for j in range(3):
                if angle[j + 1] > atom_index:
                    self.angles[i, j + 1] -= 1

        for i, dihedral in enumerate(self.dihedrals):
            for j in range(4):
                if dihedral[j + 1] > atom_index:
                    self.dihedrals[i, j + 1] -= 1

        for i, improper in enumerate(self.impropers):
            for j in range(5):
                if improper[j + 1] > atom_index:
                    self.impropers[i, j + 1] -= 1

        # check dictionaries
        print('If all of one type are removed, the type will remain in the system '
              'The type needs to be removed and shifted manually.')


    def delete_bond(self, index):
        self.bonds = numpy.delete(self.bonds, index, axis=0)
        self.bonds_comments = numpy.delete(self.bonds_comments, index)


    def delete_angle(self, index):
        self.angles = numpy.delete(self.angles, index, axis=0)
        self.angles_comments = numpy.delete(self.angles_comments, index)


    def delete_dihedral(self, index):
        self.dihedrals = numpy.delete(self.dihedrals, index, axis=0)
        self.dihedrals_comments = numpy.delete(self.dihedrals_comments, index)


    def delete_improper(self, index):
        self.impropers = numpy.delete(self.impropers, index, axis=0)
        self.impropers_comments = numpy.delete(self.impropers_comments, index)


    def new_type(self, type=0, symb='X', mass=0):

        if type in self.mass_dict.keys():
            print('Type already exists, not doing anything\r', end='')
        else:
            self.mass_dict[type] = mass
            self.type_dict[type] = symb


    def insert_atom(self, atom=Atom(), symb=None, molid=int(), index=int(), type=int(), mass=float(), charge=float(),
                 pos=numpy.array([], dtype=float), vel=numpy.array([], dtype=float), force=numpy.array([], dtype=float),
                    comment=None):

        if type:
            type_ = type
        elif atom.type:
            type_ = atom.type
        else:
            type_ = self.n_atomTypes() + 1

        if symb:
            symb_ = symb
        elif atom.symb:
            symb_ = atom.symb
        else:
            symb_ = 'X'

        if mass:
            mass_ = mass
        elif atom.mass:
            mass_ = atom.mass
        else:
            mass_ = 0

        self.new_type(type_, symb_, mass_)

        if molid:
            self.molids = numpy.hstack((self.molids, molid))
        elif atom.molid:
            self.molids = numpy.hstack((self.molids, atom.molid))
        else:
            self.molids = numpy.hstack((self.molids, self.n_molTypes() + 1))

        if index:
            self.indices = numpy.hstack((self.indices, index))
        elif atom.index:
            self.indices = numpy.hstack((self.indices, atom.index))
        else:
            self.indices = numpy.hstack((self.indices, self.n_atoms() + 1))

        if type:
            self.types = numpy.hstack((self.types, type))
        elif atom.type:
            self.types = numpy.hstack((self.types, atom.type))
        else:
            self.types = numpy.hstack((self.types, self.n_atomTypes() + 1))

        if mass:
            self.masses = numpy.hstack((self.masses, mass))
        elif atom.mass:
            self.masses = numpy.hstack((self.masses, atom.mass))
        else:
            self.masses = numpy.hstack((self.masses, 0))

        if charge:
            self.charges = numpy.hstack((self.charges, charge))
        elif atom.charge:
            self.charges = numpy.hstack((self.charges, atom.charge))
        else:
            self.charges = numpy.hstack((self.charges, 0))

        if pos.size > 0:
            self.pos = numpy.vstack((self.pos, pos))
        elif atom.pos.size > 0:
            self.pos = numpy.vstack((self.pos, atom.pos))
        else:
            self.pos = numpy.vstack((self.pos, numpy.zeros(shape=(1, 3), dtype=float)))

        if vel.size > 0:
            self.vels = numpy.vstack((self.vels, vel))
        elif atom.vel.size > 0:
            self.vels = numpy.vstack((self.vels, atom.vel))
        else:
            self.vels = numpy.vstack((self.vels, numpy.zeros(shape=(1, 3), dtype=float)))

        if force.size > 0:
            self.forces = numpy.vstack((self.forces, force))
        elif atom.force.size > 0:
            self.forces = numpy.vstack((self.forces, atom.force))
        else:
            self.forces = numpy.vstack((self.forces, numpy.zeros(shape=(1, 3), dtype=float)))

        if comment:
            self.atoms_comments = numpy.hstack((self.atoms_comments, comment))
        elif atom.comment:
            self.atoms_comments = numpy.hstack((self.atoms_comments, atom.comment))
        elif symb:
            self.atoms_comments = numpy.hstack((self.atoms_comments, symb))
        elif atom.symb:
            self.atoms_comments = numpy.hstack((self.atoms_comments, atom.symb))
        else:
            self.atoms_comments = numpy.hstack((self.atoms_comments, None))



    # def insert_atom(self, symb=None, molid=0, type=0, mass=0, charge=0, pos=numpy.array([0, 0, 0]), vel=numpy.array([0, 0, 0]), comment='None'):
    #
    #
    #     self.new_type(type, symb, mass)
    #     if molid == 0:
    #         molid = numpy.max(self.molids)+1
    #     if type == 0:
    #         type = numpy.max(self.types)+1
    #
    #     self.indices = numpy.hstack((self.indices, self.n_atoms()+1))
    #     self.molids = numpy.hstack((self.molids, molid))
    #     self.types = numpy.hstack((self.types, type))
    #     self.charges = numpy.hstack((self.charges, charge))
    #     self.pos = numpy.vstack((self.pos, pos.reshape(1, 3)))
    #     self.vels = numpy.vstack((self.vels, vels.reshape(1, 3)))
    #     self.masses = numpy.hstack((self.masses, mass))
    #     self.atoms_comments = numpy.hstack((self.atoms_comments, comment))


    def insert_bond(self, type, index1, index2):
        self.bonds = numpy.vstack((self.bonds, numpy.array([type, index1, index2])))

    def insert_angle(self, type, index1, index2, index3):
        self.angles = numpy.vstack((self.angles, numpy.array([type, index1, index2, index3])))

    def change_atomType(self, oldtype, newtype):

        self.types = numpy.where(self.types == oldtype, newtype, self.types)
        if oldtype in self.mass_dict:
            self.mass_dict[newtype] = self.mass_dict[oldtype]
            del self.mass_dict[oldtype]
        if oldtype in self.type_dict:
            self.type_dict[newtype] = self.type_dict[oldtype]
            del self.type_dict[oldtype]

    def change_bondType(self, oldtype, newtype):

        self.bonds[:, 0] = numpy.where(self.bonds[:, 0] == oldtype, newtype, self.bonds[:, 0])

    def change_angleType(self, oldtype, newtype):

        self.angles[:, 0] = numpy.where(self.angles[:, 0] == oldtype, newtype, self.angles[:, 0])

    def change_dihedralType(self, oldtype, newtype):

        self.dihedrals[:, 0] = numpy.where(self.dihedrals[:, 0] == oldtype, newtype, self.dihedrals[:, 0])

    def change_improperType(self, oldtype, newtype):

        self.impropers[:, 0] = numpy.where(self.impropers[:, 0] == oldtype, newtype, self.impropers[:, 0])


    def wrap(self):
        """Wrap coordinates into box"""
        for i, coords in enumerate(self.pos):
            for k, c in enumerate(coords):
                if c < self.box.mins[k]:
                    n = math.floor((self.box.mins[k] - c)/self.box.lengths[k]) + 1
                    self.pos[i, k] += self.box.lengths[k] * n
                elif c > self.box.maxs[k]:
                    n = math.floor((c - self.box.maxs[k])/self.box.lengths[k]) + 1
                    self.pos[i, k] -= self.box.lengths[k] * n

    def calc_center(self):
        return numpy.sum(self.pos, axis=0)/self.n_atoms()

    def combine_systems(self, molecule, box=None, center=numpy.zeros(shape=3, dtype='float')):

        if self.n_atoms() == 0:

            shift = center - molecule.calc_center()
            pos = molecule.pos + shift

            # simulation box
            self.box = molecule.box

            # mass
            self.mass_dict = molecule.mass_dict
            self.type_dict = molecule.type_dict

            # per atom
            self.indices = molecule.indices
            self.molids = molecule.molids
            self.types = molecule.types

            self.charges = molecule.charges
            self.pos = pos
            self.vels = molecule.vels
            self.forces = molecule.forces

            self.masses = molecule.masses

            # connectivity
            self.bonds = molecule.bonds
            self.angles = molecule.angles
            self.dihedrals = molecule.dihedrals
            self.impropers = molecule.impropers

            # comments
            self.atoms_comments = molecule.atoms_comments
            self.bonds_comments = molecule.bonds_comments
            self.angles_comments = molecule.angles_comments
            self.dihedrals_comments = molecule.dihedrals_comments
            self.impropers_comments = molecule.impropers_comments

        else:

            if box:
                self.box = box

            for type in molecule.types:
                self.new_type(type, molecule.type_dict[type], molecule.mass_dict[type])

            shift = center - molecule.calc_center()
            pos = molecule.pos + shift


            # connectivity
            bonds = copy.deepcopy(molecule.bonds)
            bonds[:, 1:] += self.n_atoms()
            angles = copy.deepcopy(molecule.angles)
            angles[:, 1:] += self.n_atoms()
            dihedrals = copy.deepcopy(molecule.dihedrals)
            dihedrals[:, 1:] += self.n_atoms()
            impropers = copy.deepcopy(molecule.impropers)
            impropers[:, 1:] += self.n_atoms()

            self.bonds = numpy.vstack((self.bonds, bonds))
            self.angles = numpy.vstack((self.angles, angles))
            self.dihedrals = numpy.vstack((self.dihedrals, dihedrals))
            self.impropers = numpy.vstack((self.impropers, impropers))

            # per atom
            self.indices = numpy.hstack((self.indices, molecule.indices+self.n_atoms()))
            self.molids = numpy.hstack((self.molids, molecule.molids+self.n_molTypes()))
            self.types = numpy.hstack((self.types, molecule.types))
            self.charges = numpy.hstack((self.charges, molecule.charges))
            self.pos = numpy.vstack((self.pos, pos))
            self.vels = numpy.vstack((self.vels, molecule.vels))
            self.forces = numpy.vstack((self.forces, molecule.forces))
            self.masses = numpy.hstack((self.masses, molecule.masses))
            self.atoms_comments = numpy.hstack((self.atoms_comments, molecule.atoms_comments))


            # comments
            self.bonds_comments = numpy.hstack((self.bonds_comments, molecule.bonds_comments))
            self.angles_comments = numpy.hstack((self.angles_comments, molecule.angles_comments))
            self.dihedrals_comments = numpy.hstack((self.dihedrals_comments, molecule.dihedrals_comments))
            self.impropers_comments = numpy.hstack((self.impropers_comments, molecule.impropers_comments))




# read lammps data

def read_lammpsdata(filename, style='default'):
    # TODO: improve robustness of xlo regex
    directives = re.compile(r"""
        ((?P<n_atoms>\s*\d+\s+atoms)
        |
        (?P<n_bonds>\s*\d+\s+bonds)
        |
        (?P<n_angles>\s*\d+\s+angles)
        |
        (?P<n_dihedrals>\s*\d+\s+dihedrals)
        |
        (?P<n_impropers>\s*\d+\s+impropers)
        |
        (?P<box>.+xlo)
        |
        (?P<Masses>\s*Masses)
        |
        (?P<PairCoeffs>\s*Pair\sCoeffs)
        |
        (?P<BondCoeffs>\s*Bond\sCoeffs)
        |
        (?P<AngleCoeffs>\s*Angle\sCoeffs)
        |
        (?P<DihedralCoeffs>\s*Dihedral\sCoeffs)
        |
        (?P<ImproperCoeffs>\s*Improper\sCoeffs)
        |
        (?P<Atoms>\s*Atoms)
        |
        (?P<Velocities>\s*Velocities)
        |
        (?P<Bonds>\s*Bonds)
        |
        (?P<Angles>\s*Angles)
        |
        (?P<Dihedrals>\s*Dihedrals)
        |
        (?P<Impropers>\s*Impropers))
        """, re.VERBOSE)

    # load empy counters
    n_atoms = 0
    n_bonds = 0
    n_angles = 0
    n_dihedrals = 0
    n_impropers = 0

    # data = System()

    # Read file into lines
    print("Reading '" + filename + "'")
    with open(filename, 'r') as f:
        data_lines = f.readlines()

    # iterate through every line
    i = 0
    while i < len(data_lines):
        match = directives.match(data_lines[i])
        if match:

            # initiate the class
            if match.group('n_atoms'):
                fields = data_lines.pop(i).split()
                n_atoms = int(fields[0])
                # data.update(n_atoms=int(fields[0]))

            elif match.group('n_bonds'):
                fields = data_lines.pop(i).split()
                n_bonds = int(fields[0])
                # data.update(n_bonds=int(fields[0]))

            elif match.group('n_angles'):
                fields = data_lines.pop(i).split()
                n_angles = int(fields[0])
                # data.update(n_angles=int(fields[0]))

            elif match.group('n_dihedrals'):
                fields = data_lines.pop(i).split()
                n_dihedrals = int(fields[0])
                # data.update(n_dihedrals=int(fields[0]))

            elif match.group('n_impropers'):
                fields = data_lines.pop(i).split()
                n_impropers = int(fields[0])
                # data.update(n_impropers=int(fields[0]))

            elif match.group('box'):

                # initiate our atoms class
                data = System(n_atoms=n_atoms, n_bonds=n_bonds, n_angles=n_angles, n_dihedrals=n_dihedrals,
                              n_impropers=n_impropers)

                # write box dimension to class
                for j in range(3):
                    fields = [float(x) for x in data_lines.pop(i).split()[:2]]
                    data.box.mins[j] = fields[0]
                    data.box.maxs[j] = fields[1]

                data.box.lengths = data.box.maxs - data.box.mins
                data.box.centers = (data.box.maxs + data.box.mins) / 2

            elif match.group('Masses'):

                # pop 'Masses'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.mass_dict[int(fields[0])] = float(fields[1])

                    # Add comments to type_dcit
                    if len(fields) == 4:
                        data.type_dict[int(fields[0])] = str(fields[3])

            elif match.group('Atoms'):

                # pop 'Atoms'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    line = data_lines.pop(i).split()

                    # check for comments
                    if any(field == '#' for field in line) == True:
                        commentfield = line.index('#')
                        data.atoms_comments[int(line[0]) - 1] = ' '.join(line[commentfield + 1:])
                        fields = line[:commentfield]
                    else:
                        fields = line

                    # possibilities supported
                    # index molid typeid charge xu yu zu
                    # index typeid charge xu yu zu 'for reax'

                    # initiate atom index counter
                    a_id = int(fields[0])
                    if style == 'default':

                        # normal style (no flags supported)
                        if len(fields) == 7:
                            data.indices[a_id - 1] = a_id
                            data.molids[a_id - 1] = int(fields[1])
                            data.types[a_id - 1] = int(fields[2])
                            data.charges[a_id - 1] = float(fields[3])
                            if data.mass_dict:
                                data.masses[a_id - 1] = data.mass_dict[int(fields[2])]
                            data.pos[a_id - 1] = numpy.array([float(fields[4]), float(fields[5]), float(fields[6])])

                        # normal with flags
                        elif len(fields) == 10:
                            data.indices[a_id - 1] = a_id
                            data.molids[a_id - 1] = int(fields[1])
                            data.types[a_id - 1] = int(fields[2])
                            data.charges[a_id - 1] = float(fields[3])
                            if data.mass_dict:
                                data.masses[a_id - 1] = data.mass_dict[int(fields[2])]
                            data.pos[a_id - 1] = numpy.array([float(fields[4]), float(fields[5]), float(fields[6])])

                    elif style == 'reax':
                        # reax style with no molecule id
                        if len(fields) == 6:
                            data.indices[a_id - 1] = a_id
                            data.molids[a_id - 1] = int(0)
                            data.types[a_id - 1] = int(fields[1])
                            data.charges[a_id - 1] = float(fields[2])
                            if data.mass_dict:
                                data.masses[a_id - 1] = data.mass_dict[int(fields[1])]
                            data.pos[a_id - 1] = numpy.array([float(fields[3]), float(fields[4]), float(fields[5])])

                            # reax style with no molecule id, with flags
                        elif len(fields) == 9:
                            data.indices[a_id - 1] = a_id
                            data.molids[a_id - 1] = int(0)
                            data.types[a_id - 1] = int(fields[1])
                            data.charges[a_id - 1] = float(fields[2])
                            if data.mass_dict:
                                data.masses[a_id - 1] = data.mass_dict[int(fields[1])]
                            data.pos[a_id - 1] = numpy.array([float(fields[3]), float(fields[4]), float(fields[5])])




            elif match.group('Velocities'):

                # pop 'Velocities'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.vels[int(fields[0]) - 1] = numpy.array([float(fields[1]), float(fields[2]), float(fields[3])])

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS = True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS = False

                    if COMMENTS:
                        try:
                            data.vels_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield + 1:])
                        except:
                            pass


            elif match.group('Bonds'):

                # pop 'Bonds'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.bonds[int(fields[0]) - 1] = list(map(int, fields[1:4]))

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS = True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS = False

                    if COMMENTS:
                        try:
                            data.bonds_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield + 1:])
                        except:
                            pass


            elif match.group('Angles'):

                # pop 'Angles'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.angles[int(fields[0]) - 1] = list(map(int, fields[1:5]))

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS = True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS = False

                    if COMMENTS:
                        try:
                            data.angles_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield + 1:])
                        except:
                            pass

            elif match.group('Dihedrals'):

                # pop 'Dihedrals'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.dihedrals[int(fields[0]) - 1] = list(map(int, fields[1:6]))

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS = True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS = False

                    if COMMENTS:
                        try:
                            data.dihedrals_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield + 1:])
                        except:
                            pass

            elif match.group('Impropers'):

                # pop 'Impropers'
                data_lines.pop(i)

                # eliminate remaining empty lines
                while not data_lines[i].strip():
                    data_lines.pop(i)

                while i < len(data_lines) and data_lines[i].strip():
                    fields = data_lines.pop(i).split()
                    data.impropers[int(fields[0]) - 1] = list(map(int, fields[1:7]))

                    # check for comments
                    if any(field == '#' for field in fields) == True:
                        COMMENTS = True
                        commentfield = fields.index('#')

                    else:
                        COMMENTS = False

                    if COMMENTS:
                        try:
                            data.impropers_comments[int(fields[0]) - 1] = ' '.join(fields[commentfield + 1:])
                        except:
                            pass

            else:
                i += 1
        else:
            i += 1

    return data


# read lammps trj data
def read_frame_lammpstrj(trj, read_velocities=False, read_forces=False):

    system = System()

    # Initialize variables
    box_dims = numpy.empty(shape=(3, 2))

    # --- begin header ---
    trj.readline()  # text "ITEM: TIMESTEP"
    step = int(trj.readline())  # timestep
    trj.readline()  # text "ITEM: NUMBER OF ATOMS"
    n_atoms = int(trj.readline())  # num atoms

    system.update(n_atoms=n_atoms)

    trj.readline()  # text "ITEM: BOX BOUNDS pp pp pp"
    box_dims[0] = trj.readline().split()  # x-dim of box
    box_dims[1] = trj.readline().split()  # y-dim of box
    box_dims[2] = trj.readline().split()  # z-dim of box

    system.box.mins = box_dims[:, 0]
    system.box.maxs = box_dims[:, 1]
    system.box.lengths = system.box.maxs - system.box.mins

    # assign columns
    header = trj.readline()  # text

    type_col = []
    pos_col = []
    vel_col = []
    force_col = []
    charge_col = []

    for i, item in enumerate(header.split(' ')):

        if item.find('type') == 0:
            type_col.append(i - 2)
        elif item.find('q') == 0:
            charge_col.append(i-2)
        elif item.find('x') == 0 or item.find('y') == 0 or item.find('z') == 0:
            pos_col.append(i-2)
        elif item.find('vx') == 0 or item.find('vy') == 0 or item.find('vz') == 0:
            vel_col.append(i-2)
        elif item.find('fx') == 0 or item.find('fy') == 0 or item.find('fz') == 0:
            force_col.append(i-2)


    # --- end header ---

    # --- begin body ---
    for i in range(n_atoms):
        fields = trj.readline().split()
        a_ID = int(fields[0])  # atom ID
        system.indices[a_ID - 1] = a_ID

        if type_col:
            system.types[a_ID - 1] = int(fields[type_col[0]])

        if charge_col:
            system.charges[a_ID - 1] = float(fields[charge_col[0]])

        if pos_col:
            system.pos[a_ID - 1, :] = [float(x) for x in [fields[j] for j in pos_col]]  # coordinates

        if read_velocities and vel_col:
            system.vels[a_ID - 1] = [float(x) for x in [fields[j] for j in vel_col]]  # velocities

        if read_forces and force_col:
            system.forces[a_ID - 1] = [float(x) for x in [fields[j] for j in force_col]]  # forces

    # --- end body ---

    return step, system



def read_lammpstrj(filename, read_velocities=False, read_forces=False, max_size=2):

    trajectory = {}

    statinfo = os.stat(filename)
    file_size = statinfo.st_size/1e9
    memory_available = psutil.virtual_memory().available/1e9
    memory_total = psutil.virtual_memory().total / 1e9
    print('File size is %.1f Gbytes.' % file_size)
    if file_size > max_size or file_size > memory_total*0.8:
        print('File size exceeds %.1f Gbytes.' % max_size)
        print('Total memory available: %.1f' % memory_available)
        print('Current memory available: %.1f' % memory_available)
        print('Allowing for a maximum of 80\% memory usage')
        print('Usage of function is not safe.')
        print('Adjust file size with max_size= in Gbytes')
        print('Not returning anything.')
        return trajectory

    READING = True
    frame = 0
    # loop through trj frames
    with open(filename, 'r') as f:

        # read data
        while READING:
            try:
                step, system = read_frame_lammpstrj(f, read_velocities=read_velocities, read_forces=read_forces)
                system.name = 'timestep %s' % step
                trajectory[step] = system
                frame += 1
                print('Reading timestep/frame %10d/%4i\r' % (step, frame), end='')
            except:
                READING = False

    print('')


    return trajectory


# find frame
# or
# find step

# write lammps data
def write_lammpsdata(filename, data, style='default', vels=False, masses=True):

    with open(filename, 'w') as f:
        f.write('LAMMPS data file generated with write_lammpsdata from private PyMD library. Ask author for python script m.f.dopke@tudelft.nl\n')
        f.write('\n')

        # write counters
        f.write(str(data.n_atoms()) + ' atoms\n')
        if style == 'default':
            f.write(str(data.n_bonds()) + ' bonds\n')
            f.write(str(data.n_angles()) + ' angles\n')
            f.write(str(data.n_dihedrals()) + ' dihedrals\n')
            f.write(str(data.n_impropers()) + ' impropers\n')
        f.write('\n')

        f.write(str(data.n_atomTypes()) + ' atom types\n')
        if style == 'default':
            if data.n_bonds() > 0:
                f.write(str(data.n_bondTypes()) + ' bond types\n')
            if data.n_angles() > 0:
                f.write(str(data.n_angleTypes()) + ' angle types\n')
            if data.n_dihedrals() > 0:
                f.write(str(data.n_dihedralTypes()) + ' dihedral types\n')
            if data.n_impropers() > 0:
                f.write(str(data.n_improperTypes()) + ' improper types\n')
        f.write('\n')

        # write box
        f.write('% 8.4f % 8.4f xlo xhi\n' % (data.box.mins[0], data.box.maxs[0]))
        f.write('% 8.4f % 8.4f ylo yhi\n' % (data.box.mins[1], data.box.maxs[1]))
        f.write('% 8.4f % 8.4f zlo zhi\n' % (data.box.mins[2], data.box.maxs[2]))

        if masses:
            f.write('\n')
            f.write('Masses\n')
            f.write('\n')
            for i, value in sorted(data.mass_dict.items()):
                if i in data.type_dict.keys():
                    f.write("% 4d % 8.4f # %s\n" % (i, value, data.type_dict[i]))
                else:
                    f.write("% 4d % 8.4f\n" % (i, value))

        # write atoms
        f.write('\n')
        f.write('Atoms\n')
        f.write('\n')
        for i, coord in enumerate(data.pos):

            # style default
            if style == 'default':

                # check for presence of molid
                if numpy.sum(data.molids) > 0:
                    resid = data.molids[i]
                else:
                    resid = 1

                # write default
                f.write('% 6d % 6d % 6d % 6.8f % 8.3f % 8.3f % 8.3f'
                        % (data.indices[i],
                           resid,
                           data.types[i],
                           data.charges[i],
                           coord[0],
                           coord[1],
                           coord[2]))

            # reax style without molid
            elif style == 'reax':

                f.write('% 6d % 6d % 6.8f % 8.3f % 8.3f % 8.3f'
                        % (data.indices[i],
                           data.types[i],
                           data.charges[i],
                           coord[0],
                           coord[1],
                           coord[2]))

            # check if comments are available
            if data.atoms_comments[i]:
                f.write(' # %s' % (data.atoms_comments[i]))

            # make newline
            f.write('\n')

        if vels:
            if data.vels.size:
                f.write('\n')
                f.write('Velocities\n')
                f.write('\n')
                for i, coord in enumerate(data.vels):
                    f.write('% 6d % 8.5f % 8.5f % 8.5f'
                            % (i + 1,
                               coord[0],
                               coord[1],
                               coord[2]))

                    if data.vels_comments[i]:
                        f.write(' # %s' % (data.vels_comments[i]))
                    f.write('\n')

        if style == 'default':
            if data.n_bonds() > 0:
                f.write('\n')
                f.write('Bonds\n')
                f.write('\n')
                for i, bond in enumerate(data.bonds):
                    f.write('% 6d % 6d % 6d % 6d'
                            % (i + 1,
                               int(list(map(str, bond))[0]),
                               int(list(map(str, bond))[1]),
                               int(list(map(str, bond))[2])))

                    # check for comments
                    if data.bonds_comments[i]:
                        f.write(' # %s' % (data.bonds_comments[i]))
                    f.write('\n')

            if data.n_angles() > 0:
                f.write('\n')
                f.write('Angles\n')
                f.write('\n')
                for i, angle in enumerate(data.angles):
                    f.write('% 6d % 6d % 6d % 6d % 6d'
                            % (i + 1,
                               int(list(map(str, angle))[0]),
                               int(list(map(str, angle))[1]),
                               int(list(map(str, angle))[2]),
                               int(list(map(str, angle))[3])))

                    if data.angles_comments[i]:
                        f.write(' # %s' % (data.angles_comments[i]))
                    f.write('\n')

            if data.n_dihedrals() > 0:
                f.write('\n')
                f.write('Dihedrals\n')
                f.write('\n')
                for i, dihedral in enumerate(data.dihedrals):
                    f.write('% 6d % 6d % 6d % 6d % 6d % 6d'
                            % (i + 1,
                               int(list(map(str, dihedral))[0]),
                               int(list(map(str, dihedral))[1]),
                               int(list(map(str, dihedral))[2]),
                               int(list(map(str, dihedral))[3]),
                               int(list(map(str, dihedral))[4])))

                    if data.dihedrals_comments[i]:
                        f.write(' # %s' % (data.dihedrals_comments[i]))
                    f.write('\n')

            if data.n_impropers() > 0:
                f.write('\n')
                f.write('Impropers\n')
                f.write('\n')
                for i, improper in enumerate(data.impropers):
                    f.write('% 6d % 6d % 6d % 6d % 6d % 6d % 6d'
                            % (i + 1,
                               int(list(map(str, improper))[0]),
                               int(list(map(str, improper))[1]),
                               int(list(map(str, improper))[2]),
                               int(list(map(str, improper))[3]),
                               int(list(map(str, improper))[4]),
                               int(list(map(str, improper))[5])))

                    if data.impropers_comments[i]:
                        f.write(' # %s' % (data.impropers_comments[i]))
                    f.write('\n')

    print("Wrote file '" + filename + "'")

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--fout', type=str)
    parser.add_argument('--ftrj', type=str)
    parser.add_argument('--style', type=str, default='default')
    parser.add_argument('--frame', type=int, help='frame to use', default=-1)

    args = parser.parse_args()


    data = read_lammpsdata(args.fin, style=args.style)

    # trajectory = read_lammpstrj(lammpstrjfile, read_velocities=False, read_forces=False, max_size=2)
    u = Universe(args.fin, args.ftrj, topology_format='DATA')

    LAST = True
    for key, ts in enumerate(u.trajectory):
        print('Reading frame %i\r' % key, end='')

        atoms = u.select_atoms('all')

        if key == args.frame:
            LAST = False
            data.pos =  atoms.positions
            data.box.lengths = atoms.dimensions[:3]
            data.box.mins = -data.box.lengths / 2
            data.box.maxs = data.box.lengths / 2

    print()

    if LAST:
        data.pos = atoms.positions
        data.box.lengths = atoms.dimensions[:3]
        data.box.mins = -data.box.lengths / 2
        data.box.maxs = data.box.lengths / 2

    write_lammpsdata(args.fout, data, vels=False, style=args.style)
