#!/usr/bin/env

import numpy
from PyMD.IO import write_lammpsdata, read_lammpsdata
from PyMD.functions import add_water, minimize
from PyMD.System import System, Box, Atom
import random
import argparse

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--fout', type=str)
    parser.add_argument('--style', type=str)
    parser.add_argument('--LC', type=float)
    parser.add_argument('--Lbox', metavar='N', type=float,
                        help='Box length')

    args = parser.parse_args()

    if args.style == None:
        args.style = 'default'


    system = System()

    # make crystal
    Na = Atom(symb='Na', type=1, mass=22.990, charge=+1)
    Cl = Atom(symb='Cl', type=2, mass=35.45, charge=+1)
    ghost = Atom(symb='ghost', type=3, mass=0, charge=0)

    GHOST = False

    Lbox = float(args.Lbox)
    LC = float(args.LC)

    num = int(numpy.ceil(Lbox / LC)) # has to be even
    if num % 2 != 0:
        num += 1
    Lbox = LC * num / 2

    print('Final box will be %.4f with %d particles' % (2*Lbox, num**3))
    q = input('Is this acceptable (yes/no)')
    if q != 'yes':
        exit()


    NA = 6.0221415e23  # 1/mol
    xyz = numpy.zeros((int(num**3), 3))
    c = 0
    index = 1
    for i in range(num):
        for j in range(num):
            for k in range(num):
                xyz = numpy.array([0, 0, 0]) + numpy.array([i, j, k]) * LC - 0.5 * (num-1) * LC
                if c % 2 == 0:
                    system.insert_atom(atom=Na, pos=xyz)
                else:
                    system.insert_atom(atom=Cl, pos=xyz)
                if GHOST:
                    system.insert_atom(atom=ghost, pos=xyz)
                    system.insert_bond(type=1, index1=index, index2=index+1)
                    index += 2
                c += 1
                if k == num - 1 and not j == num - 1:
                    c -= 1

    # final box for file
    region = Box()
    region.mins = numpy.array([-Lbox, -Lbox, -Lbox])
    region.maxs = numpy.array([Lbox, Lbox, Lbox])
    region.lengths = region.maxs - region.mins
    system.box = region

    write_lammpsdata(args.fout, system, style=args.style)