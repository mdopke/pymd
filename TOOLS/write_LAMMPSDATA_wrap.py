#!/usr/bin/env

import numpy
from PyMD.IO import write_lammpsdata, read_lammpsdata
from PyMD.functions import minimize
from PyMD.System import System, Box, Atom
import argparse
import ast

if __name__ == '__main__':

    parser = argparse.ArgumentParser()

    parser.add_argument('--fin', type=str)
    parser.add_argument('--fout', type=str)
    parser.add_argument('--instyle', type=str)
    parser.add_argument('--outstyle', type=str)


    args = parser.parse_args()

    if args.instyle == None:
        args.instyle = 'default'

    if args.outstyle == None:
        args.outstyle = 'default'

    system = read_lammpsdata(args.fin, style=args.instyle)

    #system.box = region
    system.wrap()

    write_lammpsdata(args.fout, system, vels=True, style=args.outstyle)
