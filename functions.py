#!/usr/bin/env

import numpy
import copy
import sys
import math
import os

from PyMD.IO import write_lammpsdata, read_h2o
from PyMD.System import System
from PyMD.System import Box


def add_water(density=1, box=Box(), system=System(), typeO=1, typeH=2, typeOH=1, typeHOH=1):

    '''
    Sample usage

    region = Box()
    region.maxs = numpy.ones(3) * 4
    region.mins = -numpy.ones(3) * 4
    region.lengths = region.maxs - region.mins

    system = add_water(density=1, box=region) # create a new empty system in which atoms will be placed

    system.wrap()

    write_lammpsdata('/home/maxi/Dropbox/Phd/sulfates_phosphates/test', system)

    :param density:
    :param box:
    :param system:
    :return:
    '''
    gmol = 18.01528
    NAvogadros = 6.022e23
    rho = density * 1e-24

    vol = box.lengths[0] * box.lengths[1] * box.lengths[2]
    water = math.ceil(rho * vol / gmol * NAvogadros)

    print('Adding %i water molecules' % water)

    pts = []
    fac = 10
    while len(pts) < water:
        x = numpy.linspace(box.mins[0]+2, box.maxs[0]-2, int((box.lengths[0]-2)/fac), endpoint=True)
        y = numpy.linspace(box.mins[1]+2, box.maxs[1]-2, int((box.lengths[1]-2)/fac), endpoint=True)
        z = numpy.linspace(box.mins[2]+2, box.maxs[2]-2, int((box.lengths[2]-2)/fac), endpoint=True)[::-1]

        pts = numpy.zeros((int(len(x) * len(y) * len(z)), 3))
        # print(len(pts), fac, water)
        fac -= 0.25

    c = 0
    for i in range(len(z)):
        for j in range(len(y)):
            for k in range(len(x)):
                pts[c, 0] = x[k]
                pts[c, 1] = y[j]
                pts[c, 2] = z[i]
                c += 1

    numpy.random.shuffle(pts)

    h2o = read_h2o(os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__))) +
                   '/h2o.txt')
    h2o.types[0] = typeO
    h2o.types[1] = typeH
    h2o.types[2] = typeH

    h2o.type_dict = {typeO:'O', typeH:'H'}
    h2o.mass_dict = {typeO:15.999, typeH:1.008}

    h2o.bonds[0, 0] = typeOH
    h2o.bonds[1, 0] = typeOH
    h2o.angles[0, 0] = typeHOH

    c = 0
    resid = 0
    for i in range(0, water * 3, 3):
        c += 1
        resid += 1
        print('Adding water %5d / %5d \r' % ((i + 1) / 3, water), end='')
        xyz = pts[c - 1, :]

        system.combine_systems(h2o, center=xyz)

    print('')

    return system


def calc_distance(p, pts, box):
    '''    significantly more efficient when wrapping first    '''

    # d = numpy.abs(p - pts)
    # res = numpy.where(d > 0.5 * box.lengths, box.lengths - d, d)

    d = p - pts
    n = numpy.floor((-box.lengths / 2 - d) / box.lengths) + 1
    d += box.lengths * n

    return numpy.sqrt((d ** 2).sum(axis=-1))


def minimize(system, center0, step=0.5, maxIter=200, method='SLSQP', region='global'):

    def fun(xyz, system, stop_criterion=5):
        if numpy.min(numpy.abs(calc_distance(xyz, system.pos, system.box))) >= stop_criterion:
            return stop_criterion
        else:
            return 1 / numpy.min(numpy.abs(calc_distance(xyz, system.pos, system.box)))

    if region == 'global':

        from scipy.optimize import basinhopping

        def print_fun(x, f, accepted):
            print(x, "at minimum %.4f accepted %d" % (1/f, int(accepted)))

        class MyBounds(object):
            def __init__(self, xmin, xmax):
                self.xmax = xmax
                self.xmin = xmin

            def __call__(self, **kwargs):
                x = kwargs["x_new"]
                tmax = bool(numpy.all(x <= self.xmax))
                tmin = bool(numpy.all(x >= self.xmin))

                return tmax and tmin

        class RandomDisplacementBounds(object):
            """random displacement with bounds"""

            def __init__(self, xmin, xmax, stepsize=0.5):
                self.xmin = xmin
                self.xmax = xmax
                self.stepsize = stepsize

            def __call__(self, x):
                """take a random step but ensure the new position is within the bounds"""
                while True:
                    # this could be done in a much more clever way, but it will work for example purposes
                    xnew = x + numpy.random.uniform(-self.stepsize, self.stepsize, numpy.shape(x))
                    if numpy.all(xnew < self.xmax) and numpy.all(xnew > self.xmin):
                        break
                return xnew

        take_step = RandomDisplacementBounds(system.box.mins[0], system.box.maxs[0], step)
        my_bounds = MyBounds(system.box.mins[0], system.box.maxs[0])
        res = basinhopping(fun, center0, minimizer_kwargs={'method': method, 'args': system}, niter=maxIter,
                           accept_test=my_bounds, niter_success=10)#, callback=print_fun)#take_step=take_step, interval=10, T=1.0, niter_success=10, callback=print_fun)
        if res['lowest_optimization_result']['success'] == True:
            x = res['x']
        else:
            print('Could not optimize')
            print(res)
            x = center0

    elif region == 'local':

        from scipy.optimize import minimize

        bounds = [(center0[0] - 2, center0[0] + 2),
                  (center0[1] - 2, center0[1] + 2),
                  (center0[2] - 2, center0[2] + 2)]

        res = minimize(fun, center0, args=(system), method=method, bounds=bounds)

        if res['success'] == True:
            x = res['x']
        else:
            print('Could not optimize')
            print(res)
            x = center0

    else:
        print('Could not optimize')
        x = center0

    return x
