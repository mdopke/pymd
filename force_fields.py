#!/usr/bin/env python

import numpy
import sys
import os

def LB(one, two, mode):
    if mode == 's':
        return (one+two)/2
    elif mode == 'e':
        return numpy.sqrt(one*two)

def geometric(one, two, mode):
    # although this is necessary for interface, it is almost negligible.
    # it matters for hydrogen, however there is a shake
    if mode == 's':
        return numpy.sqrt(one*two)
    elif mode == 'e':
        return numpy.sqrt(one*two)

def read_to_dictionary(filename):

    data = {}
    with open(filename, 'r') as f:
        for j, line in enumerate(f):
            fields = line.rstrip('\n').split(',')
            if filename.find('bond') != -1 or filename.find('angle') != -1:
                data[fields[0]] = [float(field) for field in fields[1:]]
            else:
                data[fields[0]] = float(fields[1])

    return data

def read_csv(filename):

    data = []
    with open(filename, 'r') as f:
        for j, line in enumerate(f):
            fields = line.rstrip('\n').split(',')
            if fields[0] == 'sigma' or fields[0] == 'epsilon':
                param = fields[0]
                atom = {}
                for i, field in enumerate(fields[1:]):
                    atom[field] = i
                data = numpy.zeros(shape=(i+1, i+1), dtype='float')
            else:
                for i, field in enumerate(fields[1:]):
                    if i+1 >= j:
                        if field.find('LB') != -1:
                            data[j-1, i] = -9
                            data[i, j-1] = -9
                        else:
                            data[j-1, i] = float(field)
                            data[i, j-1] = float(field)

    ij = data.shape
    for row in range(ij[0]):
        for col in range(ij[1]):
            if data[row, col] == -9:
                data[row, col] = LB(data[row, row], data[col, col], param[0])

    return numpy.asarray(data), atom


def read_file(filename):
    header = []
    with open(filename, 'r') as f:
        for line in f:
            header.append(line.rstrip('\n'))
    return header


class ForceField():

    def __init__(self):

        self.s = []
        self.e = []
        self.s_header = {}
        self.e_header = {}
        self.q = {}
        self.m = {}
        self.bond = {}
        self.angle = {}
        # self.README = ''

    def change_atr(self, old_atr, new_atr):
        self.q[new_atr] = self.q.pop(old_atr)
        self.m[new_atr] = self.m.pop(old_atr)
        self.e_header[new_atr] = self.e_header.pop(old_atr)
        self.s_header[new_atr] = self.s_header.pop(old_atr)


def read_ff(folder, bonded=False):

    FF = ForceField()

    # FF.README = read_file(folder + '/README')
    FF.e, FF.e_header = read_csv(folder + '/epsilon.csv')
    FF.s, FF.s_header = read_csv(folder + '/sigma.csv')
    FF.q = read_to_dictionary(folder + '/q.csv')
    FF.m = read_to_dictionary(folder + '/m.csv')

    if bonded:
        FF.bond = read_to_dictionary(folder + '/bond.csv')
        FF.angle = read_to_dictionary(folder + '/angle.csv')
    else:
        FF.bond = {}
        FF.angle = {}

    return FF

def merge_ffs(FF1, FF2):

    FF = ForceField()

    FF.e_header, FF.e = merge(FF1.e_header, FF2.e_header, FF1.e, FF2.e, 'e')
    FF.s_header, FF.s = merge(FF1.s_header, FF2.s_header, FF1.s, FF2.s, 's')
    FF.q = merge_dic(FF1.q, FF2.q)
    FF.m = merge_dic(FF1.m, FF2.m)
    FF.bond = merge_dic(FF1.bond, FF2.bond)
    FF.angle = merge_dic(FF1.angle, FF2.angle)

    return FF

def merge_dic(dic1, dic2):

    res = {}
    res.update(dic1)
    res.update(dic2)

    return res

def merge(header1, header2, data1, data2, method, mixingrule='LB'):

    # make single dictionary
    header_res = {}
    header_res.update(header1)
    header_res.update(header2)
    keys = header1.keys()
    for key in header2.keys():
        header_res[key] += len(keys)

    # make single array
    data_res = numpy.zeros(shape=(data1.shape[0] + data2.shape[0], data1.shape[1] + data2.shape[1])) - 9
    data_res[:data1.shape[0], :data1.shape[1]] = data1
    data_res[data1.shape[0]:, data1.shape[1]:] = data2

    # fill array diagonals with LB rule
    ij = data_res.shape
    for row in range(ij[0]):
        for col in range(ij[1]):
            if data_res[row, col] == -9:
                if mixingrule == 'LB':
                    data_res[row, col] = LB(data_res[row, row], data_res[col, col], method)
                elif mixingrule == 'geometric':
                    data_res[row, col] = geometric(data_res[row, row], data_res[col, col], method)


    return header_res, data_res


def write_forcefield(filename, potential, atoms, bonds, angles, FF, lam=1.0):

    f = open(filename, 'w+')

    f.write('#################### FORCE FIELDS ####################\n')
    f.write('\n')
    f.write('pair_style %s \n' % potential)
    f.write('\n')
    f.write('pair_modify mix arithmetic tail yes # ATC + Lorentz-Berthelot\n')
    if potential.find('tip4p') != -1:
        f.write('kspace_style pppm/tip4p 1e-6\n')
    else:
        f.write('kspace_style pppm 1e-6\n')

    f.write('\n')
    f.write('bond_style harmonic\n')
    f.write('angle_style harmonic\n')
    f.write('dihedral_style none\n')
    f.write('improper_style none\n')
    f.write('\n')
    f.write('# special_bonds lj 0.0 0.0 1.0 coul 0.0 0.0 1.0 dihedral no\n')
    f.write('\n')

    f.write('# Masses\n')
    f.write('# \n')
    for atr in atoms.keys():
        f.write('mass %i %f # %s\n' % (atr, FF.m[atoms[atr]], atoms[atr]))
    f.write('\n')
    f.write('# Pair Coeffs\n')
    f.write('# \n')

    for atr1 in atoms.keys():
        for atr2 in atoms.keys():
            if atr1 <= atr2:
                if potential.find('soft') != -1:
                    f.write('pair_coeff %i %i %s %f %f %f # %s %s \n' %
                            (atr1, atr2, potential.split(' ')[1],
                             FF.e[FF.e_header[atoms[atr1]], FF.e_header[atoms[atr2]]],
                             FF.s[FF.s_header[atoms[atr1]], FF.s_header[atoms[atr2]]],
                             lam, atoms[atr1], atoms[atr2]))
                else:
                    f.write('pair_coeff %i %i %f %f # %s %s\n' % (atr1, atr2,
                                                                  FF.e[FF.e_header[atoms[atr1]], FF.e_header[atoms[atr2]]],
                                                                  FF.s[FF.s_header[atoms[atr1]], FF.s_header[atoms[atr2]]],
                                                                  atoms[atr1], atoms[atr2]))
    f.write('\n')

    f.write('# Bond Coeffs\n')
    f.write('# \n')
    for atr in bonds:
        f.write('bond_coeff %d %f %f # %s\n' % (atr, FF.bond[bonds[atr]][0], FF.bond[bonds[atr]][1], bonds[atr]))
    f.write('\n')
    f.write('# Angle Coeffs\n')
    f.write('# \n')
    for atr in angles:
        f.write('angle_coeff %d %f %f # %s\n' % (atr, FF.angle[angles[atr]][0], FF.angle[angles[atr]][1], angles[atr]))
    f.write('\n')
    f.write('# Charges of the atoms\n')
    f.write('\n')
    for atr in atoms.keys():
        f.write('set type %i charge %f # %s\n' % (atr, FF.q[atoms[atr]], atoms[atr]))
    f.write('\n')
    f.close()

# sample usage

# # define dictionary of what should be in the force field
# atoms = {1: 'Ow', 2: 'Hw', 3:'Na', 4:'Cl'}
# bonds = {1:'OwHw'}
# angles = {1:'HwOwHw'}
#
# # read force fields
# FF_1 = read_ff(ff_file_1, bonded=True)
# FF_2 = read_ff(ff_file_2, bonded=True)
#
# # merge force fields
# FF = merge_ffs(FF_1, FF_2)
#
# write_forcefield(filename,
#                  'hybrid lj/cut/tip4p/long 1 2 1 1 0.1546 12.0 12.0', # functional
#                  atoms, bonds, angles, FF) # input data


